## Team Details<br>
1.Kurakula Narendra Kumar<br>
2.Rushabh Sachin Karade<br>
3.Arjun tanwar<br>
4.DESAI SHUBHAKAR<br>
5.Sai Dinesh Nukala<br>
6.Ganesh Suduleti<br>

Instructions to run the project:<br>

1. Please connect your server with your database, in our case we have used derby server and dbeaver DBMS, and change the database configuration inside application.properties files.<br>
2. Run both the microservices (rentahome and chat) silmultaneously on different ports.<br>
3. In Angular app please install Ngfilter and bootsrap by running following commands:<br>
   npm install bootstrap --save<br>
   npm install --save ng2-search-filter<br>

